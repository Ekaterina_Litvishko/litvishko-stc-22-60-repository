import java.util.ArrayList;
import java.util.Scanner;
import java.io.*;

import static java.lang.Integer.parseInt;

public class UsersRepositoryFileImpl implements UsersRepositoryFile {
    private final String userPath = "Users.txt";

    public void create(User user) {

    try (BufferedWriter writer = new BufferedWriter(new FileWriter(userPath, false))) {
        writer.write(forAdd(user));
        writer.flush();

    } catch (IOException e) {
        throw new RuntimeException(e);
    }
}

    public User findById(int id)  {

        String[] info;
        int userId;
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(userPath))) {
            String line = bufferedReader.readLine();
            while (line != null) {
                info = line.split("\\|");
                userId = parseInt(info[0]);

                if (id == userId) {
                    return new User(userId, info[1], info[2], parseInt(info[3]), Boolean.parseBoolean(info[4]));
                }
                line = bufferedReader.readLine();
            }
        } catch (IOException e) {
            System.out.println("Не удось найти человека с id: " + id);

        }
        return null;
    }

    @Override
    public void update(User user) {

        ArrayList<User> before = getUsers();
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(userPath, false))) {
            for (int i = 0; i < before.size(); i++) {
                if (before.get(i).getId() == user.getId()) {
                    writer.write(user.toString());
                } else {
                    writer.write(before.get(i).toString());
                }
                writer.newLine();
            }
        } catch (IOException e) {
            System.out.println("File not found");
        }
    }
    private ArrayList<User> getUsers() {

        ArrayList<User> users = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new FileReader(userPath))) {
            reader.lines().forEach(s -> {
                String[] row = s.split("\\|");
                User user = new User(
                        Integer.parseInt(row[0]),
                        row[1],
                        row[2],
                        parseInt(row[3]),
                        Boolean.parseBoolean(row[4]));
                users.add(user);
            });
        } catch (IOException e) {
            System.out.println("File not found");
        }
        return users;
    }
    public void delete(int id) {
        try {
            File templeFile = new File(userPath);
            BufferedWriter writer = new BufferedWriter(new FileWriter(userPath, false));
            Scanner scanner = new Scanner(new File(userPath));
                while (scanner.hasNext()) {
                    String line = scanner.nextLine();
                    String[] params = line.split("\\|");
                int currentUserId = parseInt(params[0]);
                if (currentUserId != id) {
                    writer.write(line + "\n");
                }
            }
            writer.flush();
            templeFile.delete();
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }
    private String forAdd(User user) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(user.getId());
        stringBuilder.append("|");
        stringBuilder.append(user.getFirstName());
        stringBuilder.append("|");
        stringBuilder.append(user.getLastName());
        stringBuilder.append("|");
        stringBuilder.append(user.getAge());
        stringBuilder.append("|");
        stringBuilder.append(user.getHasJob());
        stringBuilder.append("\n");
        return stringBuilder.toString();
    }
}