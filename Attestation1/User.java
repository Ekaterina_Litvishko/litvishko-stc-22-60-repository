public class User {

    private static int ID_COUNT = 0;
    private int id;
    private String firstName;
    private String lastName;
    private int age;
    private boolean hasJob;

    public User(String firstName, String lastName, int age, boolean hasJob) {

        this.id = ID_COUNT;
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.hasJob = hasJob;
        ID_COUNT++;
    }

    public User(int id, String firstName, String lastName, int age, boolean hasJob) {

        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.hasJob = hasJob;
    }


    public int getId() {

        return id;
    }

    public void setId(int id) {

        this.id = id;
    }

    public String getFirstName() {

        return firstName;
    }

    public void setFirstName(String firstName) {

        this.firstName = firstName;
    }

    public String getLastName() {

        return lastName;
    }

    public void setLastName(String lastName) {

        this.lastName = lastName;
    }

    public int getAge() {

        return age;
    }

    public void setAge(int age) {

        this.age = age;
    }

    public Boolean getHasJob() {

        return hasJob;
    }

    public void setHasJob() {

        this.hasJob = hasJob;
    }


    @Override
    public String toString() {
        return id + "|"
                + firstName + "|"
                + lastName + "|"
                + age + "|"
                + hasJob;
    }

}