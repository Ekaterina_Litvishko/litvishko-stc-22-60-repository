import java.io.File;

public class Main {
    public static void main(String[] args) {

        File file = new File("User.txt");

        UsersRepositoryFile usersRepositoryFile = new UsersRepositoryFileImpl();

        User user1 = new User("Ekaterina", "Litvishko", 25, false);
        User user2 = new User("Mikhail", "Litvishko", 27, true);
        User user3 = new User("Lydmila", "Ampleeva", 40, true);

        usersRepositoryFile.create(user1);
        usersRepositoryFile.create(user2);
        usersRepositoryFile.create(user3);

        User user = usersRepositoryFile.findById(2);

        user.setAge(18);
        user.setFirstName("Darya");
        usersRepositoryFile.update(user);
        usersRepositoryFile.delete(1);

        System.out.println(user);

    }
}

